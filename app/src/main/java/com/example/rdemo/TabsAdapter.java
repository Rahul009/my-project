package com.example.rdemo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabsAdapter extends FragmentStatePagerAdapter {
    int numoftabs;//no of tabs count
    public TabsAdapter(FragmentManager fragmentManager,int NoofTabs)//constructor to the class
    {
        super(fragmentManager);
      this.numoftabs=NoofTabs;
    }

    @Override
    public int getCount() {
        return numoftabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                Fragment1 fragment1=new Fragment1();
                return fragment1;

            case 1:
                Fragment11 fragment11=new Fragment11();
                return fragment11;
                default:
                    return null;
        }
    }
}
